package au.com.itgeeks.java8.collection;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.*;

public class CollectionSamples {

  public static void main(String[] args) {

    List<String> givenList = Arrays.asList("a", "bb", "ccc", "dd");

    List<String> resultList = givenList.stream().collect(toList());

    resultList.stream().map(String::toUpperCase).forEachOrdered(System.out::println);
    
    Set<String> resultset = givenList.stream().collect(toSet());
    
    resultset.stream().map(String::toUpperCase).forEachOrdered(System.out::println);
    
    resultset.forEach(item->System.out.println(" - " + item));
    
    resultset.forEach(System.out::println);
    
    Long result = givenList.stream().collect(counting());
    
    System.out.println(" " + result);
    
    Map<String, Integer> resultMap = givenList.stream().collect(toMap(Function.identity(), String::length));
    
    resultMap.forEach((k,v)->System.out.println("Item : " + k + " Count : " + v));
    
    Map<Integer, Set<String>> resultMap1 = givenList.stream().collect(groupingBy(String::length, toSet()));
    
    resultMap1.forEach((k,v)->System.out.println("Item : " + k + " Count : " + v));
    
    Optional<String> resultOpt = givenList.stream().collect(maxBy(Comparator.naturalOrder()));
    
    System.out.println(" " + resultOpt);
    
    resultOpt = givenList.stream().collect(minBy(Comparator.naturalOrder()));

    System.out.println(" " + resultOpt);
    
    Map<Boolean, List<String>> resultMap2 = givenList.stream().collect(partitioningBy(s -> s.length() > 2));
    
    resultMap2.forEach((k,v)->System.out.println("Item : " + k + " Count : " + v));
    
    
  }

}
