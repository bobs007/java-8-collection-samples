package au.com.itgeeks.java8.collection;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collector;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;

public class ImmutableSetCollector<T> implements Collector<T, ImmutableSet.Builder<T>, ImmutableSet<T>> {

  @Override
  public Supplier<ImmutableSet.Builder<T>> supplier() {
    return ImmutableSet::builder;
  }

  @Override
  public BiConsumer<ImmutableSet.Builder<T>, T> accumulator() {
    return (builder, t) -> builder.add(t);
  }

  @Override
  public BinaryOperator<ImmutableSet.Builder<T>> combiner() {
    return (left, right) -> {
      left.addAll(right.build());
      return left;
    };
  }

  @Override
  public Function<ImmutableSet.Builder<T>, ImmutableSet<T>> finisher() {
    return ImmutableSet.Builder::build;
  }

  @Override
  public Set<Characteristics> characteristics() {
    return EnumSet.of(Characteristics.UNORDERED);
  }

}
